import {makeStyles } from '@material-ui/core'
import AllEvent from './AllEvent/AllEvent';
import NewOpen from './NewOpen/NewOpen';
import NewShow from './NewShow/NewShow';
import Post from './Recommend/Post';
import Recommend from './Recommend/Recommend';

const useStyles = makeStyles((theme) => ({
    jss15: {
        position: 'relative',
        margin: '40px',
    },
    sectionBreak:{
        margin: '32px 0',
        border: 'none',
        borderTop:'16px solid #f3f4ff',
    }
}))

export default function Body() {

    const classes = useStyles()

    return (
        <div className={classes.jss15}>
            <NewShow subtitle='แข่งขัน'/>
            <NewOpen/>
            <hr className={classes.sectionBreak}/>
            <Recommend/>
            <hr className={classes.sectionBreak}/>
            <Post/>
            <hr className={classes.sectionBreak}/>
            <AllEvent/>
        </div>
    )
}
