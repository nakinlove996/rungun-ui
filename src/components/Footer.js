import { Grid, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import Link from '@material-ui/core/Link';
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
    jss51: {
        maxWidth: '100%',
        padding: '32px 80px',
        position: 'relative',
        background: 'black',
    },
    jss52: {
        color: '#ddd',
        fontSize: '14px',
        fontWeight: 'bold',
    },
    jss53: {
        color: '#ddd',
        lineHeight: '2em',
        paddingLeft: '0',
        margin: '14px 0 14px 0',
        fontWeight: '200',
    }
})
)

export default function Footer() {
    const classes = useStyles()
    return (
        <div className={classes.jss51}>
            <Grid container spacing={1} >
                <Grid item xs={12} sm={4} >
                    <Typography className={classes.jss52}>เริ่มต้น</Typography>
                    <ul className={classes.jss53} style={{ listStyleType: 'none' }}>
                        <li><Typography ><Link href="/" color="inherit">หน้าแรก</Link></Typography></li>
                        <li>ปฏิทินกิจกรรม</li>
                        <li>สถานะและประวัติสมัคร</li>
                        <li>ผลการแข่งขัน</li>
                    </ul>

                </Grid>
                <Grid item sm={4}>
                    <Typography className={classes.jss52}>บริษัท</Typography>
                    <ul className={classes.jss53} style={{ listStyleType: 'none' }}>
                        <li><Typography ><Link href="/" color="inherit">เกี่ยวกับเรา</Link></Typography></li>
                        <li>support@nakin.com</li>
                        <li>Line @test22</li>
                    </ul>

                </Grid>
                <Grid item sm={4}>
                    <Typography className={classes.jss52}>นัยทางกฏหมาย</Typography>
                    <ul className={classes.jss53} style={{ listStyleType: 'none' }}>
                        <li><Typography ><Link href="/" color="inherit">สงวนลิขสิทธิ์ (c) 2561 บริษัท ทดสอบ จำกัด</Link></Typography></li>
                        <li>ข้อกำหนดการใช้งาน</li>
                        <li>ข้อกำหนดความเป็นส่วนตัว</li>
                    </ul>
                </Grid>
            </Grid>
        </div>
    )
}
