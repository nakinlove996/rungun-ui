import { Box, Button, Divider, Grid, Link, makeStyles, Typography } from '@material-ui/core'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocationOnIcon from '@material-ui/icons/LocationOn';


const useStyles = makeStyles((theme) => ({
    jss15: {
        position: 'relative',
        margin: '40px',
    },
    jss341: {
        width: '100%',
        paddingTop: '66.66%',
        backgroundSize: 'cover',
        backgroundPosition: 'center',

    },
    jss342: {
        padding: '24px',
        backgroundColor: '#f5f5f5',
    },
    jss343: {
        height: '100%',
        padding: '24px',
        backgroundColor: '#fff',
    },
    jss344: {
        width: '160px',
        height: '2px',
        margin: '16px 0',
        backgroundColor: '#747691',
    },
    jss346: {
        height: '18px',
        verticalAlign: 'middle',
    },
    jss347: {
        marginTop: '25px',
    },
    
}))

export default function NewShow({subtitle}) {
    const classes = useStyles()
    return (
        <Grid container>
                <Grid item xs={12} md={6} style={{ position: 'relative' }}>
                    <div style={{ overflowX: 'hidden' }}>
                        <div style={{
                            flexDirection: 'row',
                            transition: 'all 0s ease 0s',
                            direction: 'ltr',
                            display: 'flex',
                            willChange: 'transform',
                            transform: 'translate(0%,0px)'
                        }}>
                            <div
                                aria-hidden="false"
                                data-swipeable="true"
                                style={{ width: '100%', flexShrink: '0', overflow: 'auto' }}
                            >
                                <Link href="#">
                                    <div className={classes.jss341} style={{ backgroundImage: 'url("../../assets/images/banner.jpg")' }} >
                                       
                                    </div>
                                </Link>
                            </div>
                        </div>
                    </div>
                </Grid>
                <Grid item xs={12} md={6} className={classes.jss342}>
                    <div className={classes.jss343}>
                        <Typography color="primary" variant="caption">
                            <span>
                                <Link href="#">{subtitle}</Link>
                            </span>
                        </Typography>
                        <Link href="#">
                            <Typography variant="h5" gutterBottom={true} color="textPrimary">E-ommerce Championship 2020</Typography>
                        </Link>
                        <Typography gutterBottom={true} variant='caption'>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illum mollitia, omnis ipsa consequuntur maiores incidunt eveniet iusto animi quia voluptatem. Vitae, consequuntur? Mollitia a incidunt quisquam libero, ducimus eius dolor? </Typography>
                        <Divider className={classes.jss344} />
                        <div>
                            <Typography variant="caption">
                                <CalendarTodayIcon className={classes.jss346} />
                                12 ธันวาคม 2563
                            </Typography>
                        </div>
                        <div>
                            <Typography variant="caption">
                                <LocationOnIcon className={classes.jss346} />
                                วัทยาลัยเทคนิคพะเยา
                            </Typography>
                        </div>
                        <Grid container spacing={1} className={classes.jss347}>
                            <Grid item>
                                <Button variant="outlined" href="#" color="inherit">อ่านเพิ่มเติม</Button>
                            </Grid>
                            <Grid item>
                                <Button variant="contained" href="#" color="primary">อ่านเพิ่มเติม</Button>
                            </Grid>
                        </Grid>
                    </div>
                </Grid>
            </Grid>
    )
}
