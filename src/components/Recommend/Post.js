import { Grid, makeStyles, Typography, Link } from "@material-ui/core";
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PostList from "./PostList";
import PostListR from "./PostListR";


const useStyles = makeStyles((theme) => ({
    jss43: {
        height: '80px',
    },
    jss63: {
        width: '100%',
        overflow: 'hidden',
        paddingTop: '66.6666%',
        backgroundSize: 'cover',
        backgroundPosition: 'center'


    }, jss65: {
        height: '14px',
        verticalAlign: 'middle'
    },
    lowColor: {
        color: '#778899',
    }
}))

export default function Post() {
    const classes = useStyles()
    return (
        <div>

            {/* Left */}

            <Grid container spacing={4} >

                <Grid Grid item xs={12} md={6} style={{ position: 'relative' }} >

                    <Typography noWrap variant="h4">เกาะติดสถานการณ์ โควิด-19</Typography>

                    <hr style={{ border: '1px solid #ff7961' }} />
                    <Link href="#" color="primary">
                        <div className={classes.jss63} style={{ backgroundImage: 'url("https://image.thainewsonline.co/uploads/images/md/2021/05/76ixzzsuCc6f6xYW666r.jpg?x-image-process=style/md")', marginTop: '40px' }}></div>
                    </Link>
                    <div>
                        <Typography gutterBottom>
                            <Link href="#">
                                <Typography variant="caption">
                                    COVID-19
                                </Typography>
                            </Link>
                        </Typography>
                    </div>
                    <Link href="#" >
                        <Typography color="textPrimary" variant="h4" style={{ fontWeight: '500' }}>โควิดคร่าชีวิต หญิงสุโขทัย พบติดเชื้อจากร่วมรับประทานอาหาร</Typography>
                    </Link>
                    <div>
                        <Typography variant="caption" className={classes.lowColor}>
                            <CalendarTodayIcon className={classes.jss65} />
                            15 ธันวาคม 2564
                        </Typography>
                    </div>
                    <Typography variant="caption" className={classes.lowColor}>
                        <LocationOnIcon className={classes.jss65} />
                        ทั่วประเทศไทย
                    </Typography>
                    <PostList />
                </Grid>
                
                

                {/* Right */}

                <Grid item xs={12} md={6} >

                    <Typography noWrap variant="h4">มาตรการเยียวยาโควิด-19</Typography>

                    <hr style={{ border: '1px solid #ff7961' }} />
                    <Link href="#" color="primary">
                        <div className={classes.jss63} style={{ backgroundImage: 'url("https://image.thainewsonline.co/uploads/images/md/2021/05/gHdDQ1pHYBwK0T0lU4I4.jpg?x-image-process=style/md")', marginTop: '40px' }}></div>
                    </Link>
                    <div>
                        <Typography gutterBottom>
                            <Link href="#">
                                <Typography variant="caption">
                                    SAVE COVID-19
                                </Typography>
                            </Link>
                        </Typography>
                    </div>
                    <Link href="#" >
                        <Typography color="textPrimary" variant="h4" style={{ fontWeight: '500' }}>ชาวกทม.ควรรู้! เปิดพิกัด25จุดให้บริการฉีดวัคซีนโควิดนอกโรงพยาบาล</Typography>
                    </Link>
                    <div>
                        <Typography variant="caption" className={classes.lowColor}>
                            <CalendarTodayIcon className={classes.jss65} />
                            15 ธันวาคม 2564
                        </Typography>
                    </div>
                    <Typography variant="caption" className={classes.lowColor}>
                        <LocationOnIcon className={classes.jss65} />
                        ทั่วประเทศไทย
                    </Typography>
                    <PostListR/>
                </Grid>

            </Grid>
        </div>
    )
}
