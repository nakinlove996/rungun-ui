
import PostItem from "./PostItem";


export default function PostList() {

    return (
        <div >

            <PostItem />

            <div style={{ marginBottom: '20px' }}></div>
        </div>
    )
}
