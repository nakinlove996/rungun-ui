import { Grid, Link, makeStyles, Typography } from "@material-ui/core";
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import { itemsR } from '../../data'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },

    jss65: {
        height: '14px',
        verticalAlign: 'middle'
    },

    jss63: {
        width: '100%',
        overflow: 'hidden',
        paddingTop: '66.6666%',
        backgroundSize: 'cover',
        backgroundPosition: 'center'
    },
    lowColor: {
        color: '#778899',
    },
    jss123: {
        padding: '24px'
    }
}))
export default function PostItem() {
    const classes = useStyles()
    return (
        <div>
            {
                itemsR.map((item) => (
                    <Grid container spacing={2} style={{ marginTop: '10px' }}>
                        <Grid item xs={6} sm={4}>
                            <div className={classes.jss63} style={{ backgroundImage: `url('${item.img}')` }}></div>
                        </Grid>
                        <Grid item xs={6} sm={8} container>
                            <Grid item xs container direction="column" spacing={6}>
                                <Grid item xs>
                                    <Link href="#" gutterBottom variant="caption">{item.subTitle}</Link>
                                    <Typography gutterBottom noWrap>
                                        <Link variant="body2" href="#" color="textPrimary" style={{ fontWeight: '300' }} >
                                            {item.title}
                                </Link>
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" style={{ fontWeight: '200' }} noWrap>
                                        {item.desc}
                        </Typography>
                                    <Typography variant="caption" color="textSecondary">
                                        <CalendarTodayIcon className={classes.jss65} />
                        {item.day}
                    </Typography>
                                </Grid>

                            </Grid>
                            <Grid item>
                                <Typography variant="subtitle1" style={{ cursor: 'pointer' }}>อ่านต่อ</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                ))
            }
        </div>

    )
}
