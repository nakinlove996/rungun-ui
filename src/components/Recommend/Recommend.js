import { Grid, Link, makeStyles, Typography } from "@material-ui/core";
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import ItemsRe from "./ItemsRe";

const useStyles = makeStyles((theme) => ({
    jss43: {
        height: '64px',

    },
    jss63: {
        width: '100%',
        overflow: 'hidden',
        paddingTop: '66.6666%',
        backgroundSize: 'cover',
        backgroundPosition: 'center'
    },
    jss65: {
        height: '14px',
        verticalAlign: 'middle'
    }
}));

export default function Recommend() {


    const classes = useStyles()

    return (
        <div className={classes.jss42}>
            <Grid container spacing={6}>
                <Grid item xs={12} sm={6}>
                    <div className={classes.jss43}>
                        <Typography variant="h5" style={{ fontWeight: 'bold', fontSize: '800' }}>แนะนำ</Typography>
                    </div>
                    <Link href="#" color="primary" >
                        <div className={classes.jss63} style={{ backgroundImage: 'url("https://via.placeholder.com/600x400")' }}></div>
                    </Link>
                    <div>
                        <Typography variant="caption" gutterBottom={true}>
                            <Link href="#" color="primary">
                                <Typography variant="caption" color="primary">Soon..</Typography>
                            </Link>
                        </Typography>
                        <Link href="#" color="primary" >
                            <Typography variant="subtitle1" color="textPrimary" noWrap gutterBottom={true}>Comeing Soon.... 2021 asdasdadad</Typography>
                        </Link>
                        <div>
                            <Typography variant="caption" noWrap>
                                <CalendarTodayIcon className={classes.jss65} />
                                    2-3 ตุลาคม 2564
                            </Typography>
                        </div>
                        <div>
                            <Typography variant="caption" noWrap >
                                <LocationOnIcon className={classes.jss65} />
                                    วัทยาลัยเทคนิคพะเยา (จังหวัดพะเยา)
                            </Typography>
                        </div>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Grid container spacing={6}>
                        <ItemsRe/>
                        <ItemsRe/>
                        <ItemsRe/>
                        <ItemsRe/>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}
