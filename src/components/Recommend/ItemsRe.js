import { Grid, Link, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocationOnIcon from '@material-ui/icons/LocationOn';

const useStyles = makeStyles({
    jss63: {
        width: '100%',
        overflow: 'hidden',
        position:'relative',
        paddingTop: '66.6666%',
        backgroundSize: 'cover',
        backgroundPosition: 'center'
    },
    jss65: {
        height: '14px',
        verticalAlign: 'middle'
    }
});

export default function ItemsRe() {
    const classes = useStyles()
    return (
        <Grid item xs={6}>
            <Link>
                <div className={classes.jss63} style={{ backgroundImage: 'url("https://via.placeholder.com/600x400")' }}></div>
            </Link>
            <div>
                        <Typography variant="caption" gutterBottom={true}>
                            <Link href="#" color="primary">
                                <Typography variant="caption" color="primary">Soon..</Typography>
                            </Link>
                        </Typography>
                        <Link href="#" color="primary" >
                            <Typography variant="subtitle1" color="textPrimary" noWrap gutterBottom={true}>Comeing Soon.... 2021 asdasdadad</Typography>
                        </Link>
                            <Typography variant="body2" noWrap>
                                <CalendarTodayIcon className={classes.jss65} />
                                    2-3 ตุลาคม 2564
                            </Typography>
                            <Typography  noWrap variant="body2" >
                                <LocationOnIcon className={classes.jss65} />
                                    วัทยาลัยเทคนิคพะเยา (จังหวัดพะเยา)
                            </Typography>
                    </div>
        </Grid>
    )
}
