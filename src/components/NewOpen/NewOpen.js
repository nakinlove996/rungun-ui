import { Grid, Link, makeStyles, Typography } from "@material-ui/core"
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Items from "./Items";

const useStyles = makeStyles((theme) => ({
    jss16: {
        display: 'flex',
        marginTop: '64px',
    },
    jss59: {
        width: '100%',
        overflow: 'hidden',
        position: 'relative',
        paddingTop: '66.6666%',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    jss32: {
        height: '80px'
    },
    jss35: {
        height: '80px'
    },
    jss54: {
        textDecoration: 'none',
    }, jss63: {
        color: '#ff6617',
    },
    jss61: {
        height: '14px',
        verticalAlign: 'middle'
    }
}))

export default function NewOpen() {
    const classes = useStyles()
    return (
        <div className={classes.jss16}>
            <Grid container spacing={6}>
                <Grid item xs={12} sm={6}>
                    <div className={classes.jss32}>
                        <Typography variant="h4" style={{ fontWeight: 'bold' }}>
                            เปิดใหม่
                        </Typography>
                    </div>
                    <Grid container spacing={6}>
                        <Items/>
                        <Items/>
                        <Items/>
                        <Items/>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <div >
                        <Typography variant="h4" style={{ fontWeight: 'bold' }} className={classes.jss35}>
                            ยอดนิยม
                        </Typography>
                        <Link href="#" color="primary">
                            <div className={classes.jss59} style={{ backgroundImage: 'url("https://via.placeholder.com/600x400")' }}></div>
                        </Link>
                        <Typography variant="caption" gutterBottom={true}>
                            <span className={classes.jss63}>
                                <Link href="#" color="primary" className={classes.jss54}>

                                    <Typography variant="caption" color="primary">Soon...</Typography>

                                </Link>
                            </span>
                            <Link href="#" color="primary" className={classes.jss54}>
                                <Typography noWrap variant="subtitle1" color="textPrimary" gutterBottom={true} >Comeing Soon.... 2021</Typography>
                            </Link>
                            <div>
                                <Typography variant="caption" noWrap>
                                    <CalendarTodayIcon className={classes.jss61} />
                                    2-3 ตุลาคม 2564
                            </Typography>
                            </div>
                            <div>
                                <Typography variant="caption" noWrap >
                                    <LocationOnIcon className={classes.jss61} />
                                    วัทยาลัยเทคนิคพะเยา
                            </Typography>
                            </div>
                        </Typography>
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}
