import { Grid, Link, makeStyles, Typography } from "@material-ui/core"
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocationOnIcon from '@material-ui/icons/LocationOn';

const useStyles = makeStyles((theme) => ({
    
    jss59: {
        width: '100%',
        overflow: 'hidden',
        position: 'relative',
        paddingTop: '66.6666%',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    jss54: {
        textDecoration: 'none',
    }, jss63: {
        color: '#ff6617',
    },
    jss61: {
        height: '14px',
        verticalAlign: 'middle'
    }
}))

export default function Items() {
    const classes = useStyles()
    return (
        <Grid item xs={6}>
            <Link href="#" color="primary">
                <div className={classes.jss59} style={{ backgroundImage: 'url("https://via.placeholder.com/600x400")' }}></div>
            </Link>
            <Typography variant="caption" gutterBottom={true}>
                <span className={classes.jss63}>
                    <Link href="#" color="primary" className={classes.jss54}>

                        <Typography variant="caption" color="primary">Soon...</Typography>

                    </Link>
                </span>
                <Link href="#" color="primary" className={classes.jss54}>
                    <Typography noWrap variant="subtitle1" color="textPrimary" gutterBottom={true} >Comeing Soon.... 2021</Typography>
                </Link>
                <div>
                    <Typography variant="body2" noWrap>
                        <CalendarTodayIcon className={classes.jss61} />
                                    2-3 ตุลาคม 2564
                            </Typography>
                </div>
                <div>
                    <Typography variant="body2" noWrap >
                        <LocationOnIcon className={classes.jss61} />
                                    วัทยาลัยเทคนิคพะเยา
                            </Typography>
                </div>
            </Typography>
        </Grid>
    )
}
