import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import {
    featuredPortfolio,
    webPortfolio,
    mobilePortfolio,
    designPortfolio,
    contentPortfolio
} from '../../data'
import { Grid, Link } from '@material-ui/core';


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
};

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    jss46: {
        display: 'flex',
        marginBottom: '48px',
        justifyContent: 'center'
    },
    jss59: {
        width: '100%',
        overflow: 'hidden',
        position: 'relative',
        paddingTop: '66.6666%',
        backgroundSize: 'cover',
        backgroundPosition: 'center'
    }, jss65: {
        height: '14px',
        verticalAlign: 'middle'
    }
}));

export default function ScrollableTabsButtonAuto() {
    const classes = useStyles();
    const [value, setValue] = useState(0);

    const [selected, setSelected] = useState("featured");
    const [data, setData] = useState([]);

    const list = [
        {
            id: 'featured',
            title: 'featured',
        },
        {
            id: 'web',
            title: 'web',
        },
        {
            id: 'mobile',
            title: 'mobile',
        },
        {
            id: 'design',
            title: 'Design',
        },
        {
            id: 'contant',
            title: 'Contant',
        },
        {
            id: 'soon',
            title: 'soon',
        },
        {
            id: 'soon',
            title: 'soon',
        },
        {
            id: 'soon',
            title: 'soon',
        },


    ];

    useEffect(() => {
        switch (selected) {
            case "featured":
                setData(featuredPortfolio);
                break;
            case "web":
                setData(webPortfolio);
                break;
            case "mobile":
                setData(mobilePortfolio);
                break;
            case "design":
                setData(designPortfolio);
                break;
            case "contant":
                setData(contentPortfolio);
                break;
            default:
                setData(featuredPortfolio);
        }
    }, [selected])

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <div className={classes.jss46}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    scrollButtons="on"
                    variant="scrollable"
                    aria-label="scrollable auto tabs example"
                >
                    {list.map((item) => (
                        <Tab key={item.id} label={item.title}
                            onClick={() => setSelected(item.id)}
                        />
                    ))}
                </Tabs>
            </div>
            <Grid container spacing={6}>
                {data.map((d) => (
                    <Grid item xs={6} sm={4} md={3}>
                        <Link href="#" >
                            <div className={classes.jss59} style={{ backgroundImage: `url('${d.img}')` }}></div>
                        </Link>
                        <Typography variant="caption" gutterBottom={true}>
                            <Link >
                                <Typography variant="caption" color="primary">{d.tag}</Typography>
                            </Link>
                        </Typography>
                        <Link>
                            <Typography variant="h6" noWrap gutterBottom color="textPrimary">{d.title}</Typography>
                        </Link>
                        <div>
                        <Typography variant="caption" noWrap>
                            <CalendarTodayIcon className={classes.jss65} />
                            {d.day}
                        </Typography>
                        </div>
                        <Typography variant="caption" noWrap>
                            <LocationOnIcon className={classes.jss65} />
                            {d.location}
                        </Typography>
                    </Grid>
                ))}
            </Grid>
        </div>
    );
}