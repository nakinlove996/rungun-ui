export const featuredPortfolio = [
    {
      id: 1,
      title: "Social Media App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 2,
      title: "Rampa UI Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/702789/screenshots/15054318/media/4ea5d492b7b07eebc9528ff960794879.png?compress=1&resize=1200x900",
    },
    {
      id: 3,
      title: "E-commerce Web Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/1387827/screenshots/15466426/media/deb2dca6762cd3610321c98bfccb0b72.png?compress=1&resize=1200x900",
    },
    {
      id: 4,
      title: "Relax Mobile App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/4095861/screenshots/15467417/media/d6a15c416626f12b31fa5ca1db192572.png?compress=1&resize=1200x900",
    },
    {
      id: 5,
      title: "Hero Web Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/5031392/screenshots/15467520/media/c36b3b15b25b1e190d081abdbbf947cf.png?compress=1&resize=1200x900",
    },
    {
      id: 6,
      title: "Banking App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/3307260/screenshots/15468444/media/447b4501f7a145b05e11c70199a9c417.jpg?compress=1&resize=1200x900",
    },{
      id: 7,
      title: "Banking App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/3307260/screenshots/15468444/media/447b4501f7a145b05e11c70199a9c417.jpg?compress=1&resize=1200x900",
    },{
      id: 8,
      title: "Banking App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/3307260/screenshots/15468444/media/447b4501f7a145b05e11c70199a9c417.jpg?compress=1&resize=1200x900",
    },{
      id: 9,
      title: "Banking App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/3307260/screenshots/15468444/media/447b4501f7a145b05e11c70199a9c417.jpg?compress=1&resize=1200x900",
    },
  ];
  
  export const webPortfolio = [
    {
      id: 1,
      title: "Web Social Media App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img: "https://cdn.dribbble.com/users/5031392/screenshots/15467520/media/c36b3b15b25b1e190d081abdbbf947cf.png?compress=1&resize=1200x900",
    },
    {
      id: 2,
      title: "Web Rampa UI Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img: "https://cdn.dribbble.com/users/3967258/screenshots/15463803/media/4fddb9a2caf3b3bd634060f706a91e73.png?compress=1&resize=1200x900",
    },
    {
      id: 3,
      title: "Web E-commerce Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img: "https://cdn.dribbble.com/users/3637068/screenshots/15467010/media/69a3279182b75365013fe285733d1122.png?compress=1&resize=1200x900",
    },
    {
      id: 4,
      title: "Web Relax App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img: "https://cdn.dribbble.com/users/2264844/screenshots/15463998/media/d85e92a332c41094f68e1f716884f7ce.jpg?compress=1&resize=1200x900",
    },
    {
      id: 5,
      title: "Web Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img: "https://cdn.dribbble.com/users/387658/screenshots/15458608/media/e92ec1dd33dd1170ae3dc9c5272f2693.jpg?compress=1&resize=1200x900",
    },
    {
      id: 6,
      title: "Web Banking App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img: "https://cdn.dribbble.com/users/6384483/screenshots/15468426/media/ce9479fa1f8dba3a4a49840d76e55e31.png?compress=1&resize=1200x900",
    },
  ];
  
  export const mobilePortfolio = [
    {
      id: 1,
      title: "Mobile Social Media App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/2597268/screenshots/15468893/media/44313553d73ba41580f1df69749bba28.jpg?compress=1&resize=1200x900",
    },
    {
      id: 2,
      title: "Mobile Rampa UI Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/5920881/screenshots/15463142/media/b5a460223798bd85d835710806e2f3dd.png?compress=1&resize=1200x900",
    },
    {
      id: 3,
      title: "Mobile E-commerce Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/1998175/screenshots/15459384/media/48ac2b43ebe81ba0866afea1383cc939.png?compress=1&resize=1200x900",
    },
    {
      id: 4,
      title: "Mobile Relax App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/2083704/screenshots/15468619/media/cd958306c7a772449e1ac23bd65ce506.png?compress=1&resize=1200x900",
    },
    {
      id: 5,
      title: "Mobile Hero Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/26642/screenshots/15462545/media/1a202ef5c8338b6f78634e6edc1ba350.png?compress=1&resize=1200x900",
    },
    {
      id: 6,
      title: "Mobile Banking App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://cdn.dribbble.com/users/1387827/screenshots/15466426/media/deb2dca6762cd3610321c98bfccb0b72.png?compress=1&resize=1200x900",
    },
  ];
  
  export const designPortfolio = [
    {
      id: 1,
      title: "Design Social Media App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 2,
      title: "Design Rampa UI Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 3,
      title: "Design E-commerce Web Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 4,
      title: "Design Relax Mobile App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 5,
      title: "Design Keser Web Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 6,
      title: "Design Banking App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
  ];
  
  export const contentPortfolio = [
    {
      id: 1,
      title: "Content Social Media App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 2,
      title: "Content Rampa UI Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 3,
      title: "Content E-commerce Web Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 4,
      title: "Content Relax Mobile App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 5,
      title: "Content Keser Web Design",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
    {
      id: 6,
      title: "Content Banking App",
      tag:'Soon...',
      day:'16 พฤษาคม 2563',
      location: 'จังหวัดพะเยา',
      img:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU",
    },
  ];
  

  export const items = [
    {
        id: 1,
        subTitle: 'COVID-19',
        title: 'แพทย์ขอนแก่น พ้อหนัก เผยเรื่องราวสุดเศร้า สูญเสียผู้ป่วยโควิด',
        desc: 'ผู้ใช้เฟซบุ๊กชื่อ Weerayuth Hoom-ard ซึ่งเป็นแพทย์ในโรงพยาบาลขอนแก่น โพสต์เรื่องราว ระบุว่า อีกหนึ่งชีวิต ผู้ป่วยชายวัยใกล้เกษียณ ยศ ร.ต.อ.  ที่ต้องเสียชีวิตไปจากการติดเชื้อโควิด-19 แม้ท่านจะได้ต่อสู้กับโรคร้ายนี้ร่วมกับทีมจนถึงที่สุด',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/rCqkgEFtD8QctIy08SkE.jpg?x-image-process=style/md'
    },
    {
        id: 2,
        subTitle: 'COVID-19',
        title: 'คลัสเตอร์ท่าอากาศยานสุวรรณภูมิ ติดเชื้อโควิดแล้ว 105 ราย',
        desc: 'นพ.ทวีศิลป์ วิษณุโยธิน โฆษกศูนย์บริหารสถานการณ์โควิด-19 (ศบค.) เปิดเผยว่า พบการติดเชื้อโควิด-19 ของบริษัทเอกชนในคลัสเตอร์ของพื้นที่ท่าอากาศยานสุวรรณภูมิ โดยมีผู้ติดเชื้อรวมสะสมจำนวน 105 ราย (5 เม.ย.-14 พ.ค.)',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/No0CKmIoMSFVNB1CAUGF.jpg?x-image-process=style/md'
    },
    {
        id: 3,
        subTitle: 'COVID-19',
        title: 'แพทย์ขอนแก่น พ้อหนัก เผยเรื่องราวสุดเศร้า สูญเสียผู้ป่วยโควิด',
        desc: 'ผู้ใช้เฟซบุ๊กชื่อ Weerayuth Hoom-ard ซึ่งเป็นแพทย์ในโรงพยาบาลขอนแก่น โพสต์เรื่องราว ระบุว่า อีกหนึ่งชีวิต ผู้ป่วยชายวัยใกล้เกษียณ ยศ ร.ต.อ.  ที่ต้องเสียชีวิตไปจากการติดเชื้อโควิด-19 แม้ท่านจะได้ต่อสู้กับโรคร้ายนี้ร่วมกับทีมจนถึงที่สุด',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/rCqkgEFtD8QctIy08SkE.jpg?x-image-process=style/md'
    },
    {
        id: 4,
        subTitle: 'COVID-19',
        title: 'แพทย์ขอนแก่น พ้อหนัก เผยเรื่องราวสุดเศร้า สูญเสียผู้ป่วยโควิด',
        desc: 'ผู้ใช้เฟซบุ๊กชื่อ Weerayuth Hoom-ard ซึ่งเป็นแพทย์ในโรงพยาบาลขอนแก่น โพสต์เรื่องราว ระบุว่า อีกหนึ่งชีวิต ผู้ป่วยชายวัยใกล้เกษียณ ยศ ร.ต.อ.  ที่ต้องเสียชีวิตไปจากการติดเชื้อโควิด-19 แม้ท่านจะได้ต่อสู้กับโรคร้ายนี้ร่วมกับทีมจนถึงที่สุด',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/rCqkgEFtD8QctIy08SkE.jpg?x-image-process=style/md'
    },
    {
        id: 5,
        subTitle: 'COVID-19',
        title: 'แพทย์ขอนแก่น พ้อหนัก เผยเรื่องราวสุดเศร้า สูญเสียผู้ป่วยโควิด',
        desc: 'ผู้ใช้เฟซบุ๊กชื่อ Weerayuth Hoom-ard ซึ่งเป็นแพทย์ในโรงพยาบาลขอนแก่น โพสต์เรื่องราว ระบุว่า อีกหนึ่งชีวิต ผู้ป่วยชายวัยใกล้เกษียณ ยศ ร.ต.อ.  ที่ต้องเสียชีวิตไปจากการติดเชื้อโควิด-19 แม้ท่านจะได้ต่อสู้กับโรคร้ายนี้ร่วมกับทีมจนถึงที่สุด',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/rCqkgEFtD8QctIy08SkE.jpg?x-image-process=style/md'
    },
];

export const itemsR = [
    {
        id: 1,
        subTitle: 'SAVE COVID-19',
        title: 'องค์การเภสัชกรรม เผยปลายเดือนนี้ ไทยได้วัคซีนซิโนแวคจากจีนอีก1.5 ล้านโดส',
        desc: 'องค์การเภสัชกรรม(GPO) รับวัคซีนโควิด-19 ของซิโนแวคจากจีนอีก 5 แสนโดส ปลายพฤษภาคมเข้ามาอีก 1.5 ล้านโดส',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/ecFqT5w81SrsvByR7IPE.jpg?x-image-process=style/md'
    },
    {
        id: 2,
        subTitle: 'SAVE COVID-19',
        title: 'ชุมชนริมคลองสามเสน ทนไม่ไหว วอนรัฐช่วยเหลือ โควิดระบาดมีติดเชื้อแล้ว41คน',
        desc: 'ชาวชุมชนริมคลองสามเสน วอนหน่วยงานรัฐเข้าช่วยเหลือคนในชุมชนอย่างเร่งด่วน เนื่องจากโควิด 19 ระบาดในชุมชน และมีผู้ติดเชื้อแล้วหลายราย เบื้องต้นกว่า41คน หวั่นกลายเป็นคลัสเตอร์ใหม่ของกทม.',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/pKNVP02twRV6QR0sW8Nm.jpg?x-image-process=style/md'
    },
    {
        id: 3,
        subTitle: 'SAVE COVID-19',
        title: 'แพทย์ขอนแก่น พ้อหนัก เผยเรื่องราวสุดเศร้า สูญเสียผู้ป่วยโควิด',
        desc: 'ผู้ใช้เฟซบุ๊กชื่อ Weerayuth Hoom-ard ซึ่งเป็นแพทย์ในโรงพยาบาลขอนแก่น โพสต์เรื่องราว ระบุว่า อีกหนึ่งชีวิต ผู้ป่วยชายวัยใกล้เกษียณ ยศ ร.ต.อ.  ที่ต้องเสียชีวิตไปจากการติดเชื้อโควิด-19 แม้ท่านจะได้ต่อสู้กับโรคร้ายนี้ร่วมกับทีมจนถึงที่สุด',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/rCqkgEFtD8QctIy08SkE.jpg?x-image-process=style/md'
    },
    {
        id: 4,
        subTitle: 'SAVE COVID-19',
        title: 'แพทย์ขอนแก่น พ้อหนัก เผยเรื่องราวสุดเศร้า สูญเสียผู้ป่วยโควิด',
        desc: 'ผู้ใช้เฟซบุ๊กชื่อ Weerayuth Hoom-ard ซึ่งเป็นแพทย์ในโรงพยาบาลขอนแก่น โพสต์เรื่องราว ระบุว่า อีกหนึ่งชีวิต ผู้ป่วยชายวัยใกล้เกษียณ ยศ ร.ต.อ.  ที่ต้องเสียชีวิตไปจากการติดเชื้อโควิด-19 แม้ท่านจะได้ต่อสู้กับโรคร้ายนี้ร่วมกับทีมจนถึงที่สุด',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/rCqkgEFtD8QctIy08SkE.jpg?x-image-process=style/md'
    },
    {
        id: 5,
        subTitle: 'SAVE COVID-19',
        title: 'แพทย์ขอนแก่น พ้อหนัก เผยเรื่องราวสุดเศร้า สูญเสียผู้ป่วยโควิด',
        desc: 'ผู้ใช้เฟซบุ๊กชื่อ Weerayuth Hoom-ard ซึ่งเป็นแพทย์ในโรงพยาบาลขอนแก่น โพสต์เรื่องราว ระบุว่า อีกหนึ่งชีวิต ผู้ป่วยชายวัยใกล้เกษียณ ยศ ร.ต.อ.  ที่ต้องเสียชีวิตไปจากการติดเชื้อโควิด-19 แม้ท่านจะได้ต่อสู้กับโรคร้ายนี้ร่วมกับทีมจนถึงที่สุด',
        day: '15 ธันวาคม 2564',
        img: 'https://image.thainewsonline.co/uploads/images/md/2021/05/rCqkgEFtD8QctIy08SkE.jpg?x-image-process=style/md'
    },
];