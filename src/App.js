
import React from 'react'
import Header from './components/Header'
import Footer from './components/Footer'
import './home.css'
import { Container, createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core';
import Body from './components/Body';

const useStyles = makeStyles((theme) => ({
  containerPadding: {
    padding:'0',
    width: '100%',
    overflow: 'hidden',
    position: 'relative',
  }
}))

const themes = createMuiTheme({
  palette: {
    primary: {
      light: '#ff7961',
      main: '#ff6617',
      dark: '#ba000d',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
    },
  },
});

function App() {
  const classes = useStyles()
  return (
    <ThemeProvider theme={themes} >
      <Container maxWidth="lg" className={classes.containerPadding}>
        <Header />
        <Body />
        <Footer />
      </Container>
    </ThemeProvider>
  );
}

export default App;
